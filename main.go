package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
	"time"
)

const (
	tokenEnvVar       = "GITLAB_PRIVATE_TOKEN"
	rootURLEnvVar     = "GITLAB_API_ROOT_URL"
	defaultAPIRootURL = "https://code.videolan.org/api/v4"
	ActionShow        = "show"
	ActionDummy       = "dummy"
	ActionDelete      = "delete"
	ActionStats       = "stats"
	UpdatedBefore     = "updated_before"
	UpdatedAfter      = "updated_after"
)

var (
	verbose  = false
	debug    = false
	chatty   = true
	colorful = true
)

func bold(s string) string {
	return fmt.Sprintf("\033[1m%s\033[0m", s)
}
func logStderrln(v ...interface{}) {
	fmt.Fprintln(os.Stderr, v...)
}
func logStdoutln(v ...interface{}) {
	fmt.Fprintln(os.Stdout, v...)
}

func arrayOptionToMap(array []string) map[string]string {
	m := map[string]string{}
	for _, a := range array {
		if !strings.Contains(a, "=") {
			log.Println("The following filter does not respect the key=value syntax ", a)
			os.Exit(-1)
		}
		s := strings.SplitN(a, "=", 2)
		m[s[0]] = s[1]
	}
	return m
}

func checkDate(m map[string]string, key string) {
	if t, ok := m[key]; ok {
		if _, err := time.Parse(time.RFC3339, t); err != nil {
			log.Println("Cannot Parse date ", key, ":", err.Error())
			log.Println("Date format is RFC3339, examples: '2021-11-01T00:00:00Z' for UTC , '2019-10-12T07:20:50.52+02:00' for UTC+2 ")
			os.Exit(-1)
		}
	}
}

type UserCount struct {
	User  User
	Count int
}

type Item struct {
	Key, Value int64
}

func sortMap(m map[int64]int64) []Item {
	list := []Item{}
	for k, v := range m {
		list = append(list, Item{k, v})
	}
	sort.Slice(list, func(i, j int) bool {
		return list[i].Value > list[j].Value
	})
	return list
}

// Handle multiple times the same filter flag

type flagArray []string

func (f *flagArray) String() string {
	var array []string = []string(*f)
	return fmt.Sprint(array)
}

func (f *flagArray) Set(value string) error {
	*f = append(*f, strings.TrimSpace(value))
	return nil
}

func askConfirm(message string) bool {
	log.Println(message, " (y/N): ")
	log.Print(" ")
	var s string
	_, err := fmt.Scan(&s)
	if err != nil {
		panic(err)
	}
	s = strings.TrimSpace(s)
	s = strings.ToLower(s)
	if s == "y" || s == "yes" {
		return true
	}
	return false
}

func main() {
	var projectID int64
	var maxParallel int
	var action string
	var filters flagArray
	var actionOptions flagArray
	var on string
	flag.Int64Var(&projectID, "project", 0, "The Gitlab Project ID. Look at the Project Home Page")
	flag.BoolVar(&verbose, "verbose", false, "Verbose mode.")
	flag.BoolVar(&debug, "debug", false, "Debug mode. Shows HTTP requests")
	flag.BoolVar(&colorful, "colorful", true, "Rainbow logs!")
	flag.IntVar(&maxParallel, "maxParallel", 5, "max number of coroutines in parallel")
	flag.Var(&filters, "filter", "filters. Syntax: -filter key1=value1 -filter key2=value2 ... Keys are listed here: https://docs.gitlab.com/ee/api/issues.html#list-issues")
	flag.StringVar(&on, "on", "issues", "Perform action on project 'issues' | 'mrs'.")
	flag.StringVar(&action, "action", "show", "Action to perform on issues: show, ")
	flag.Var(&actionOptions, "option", "Action options. Syntax: -option key1=value1 -filter key2=value2")
	// Timeout/duration parameters for checks
	flag.Parse()
	if projectID == 0 {
		log.Println("Please provide a Project ID.")
		os.Exit(-1)
	}
	token := os.Getenv(tokenEnvVar)
	if token == "" {
		log.Println(tokenEnvVar, "environment variable not set! Aborting...")
		os.Exit(-1)
	}
	rootURL := os.Getenv(rootURLEnvVar)
	if rootURL == "" {
		rootURL = defaultAPIRootURL
	}
	// Get connected user
	user, err := GetConnectedUser(rootURL, token)
	if err != nil {
		log.Println("Error while retrieving Connected user information: ", err.Error())
		os.Exit(-1)
	}
	log.Println("Connected as", user.Username, "(", user.Name, ")")
	project := Project{ID: projectID, APIRootURL: rootURL, AuthToken: token}
	switch on {
	case "issues":
		ActOnIssues(&project, arrayOptionToMap(filters), maxParallel, action, arrayOptionToMap(actionOptions))
	case "mrs":
		ActOnMergeRequests(&project, arrayOptionToMap(filters), maxParallel, action, arrayOptionToMap(actionOptions))
	default:
		log.Println("Unknown category: ", on)
		os.Exit(-1)
	}
}
