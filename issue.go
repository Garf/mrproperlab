package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"sync"
	"time"
)

func logIssue(mrID int64, v ...interface{}) {
	header := fmt.Sprintf("[Issue %d]", mrID)
	if colorful {
		color := mrID % 256
		header = fmt.Sprintf("\033[38;5;%dm[Issue %d]\033[39;49m", color, mrID)
	}
	logStderrln(header, fmt.Sprint(v...))
}
func logIssueVerbose(mrID int64, v ...interface{}) {
	if verbose || debug {
		logMR(mrID, v...)
	}
}

type IssueAction interface {
	Action(*sync.WaitGroup, Issue, map[string]string, map[string]string, chan interface{})
	Result([]interface{}, map[string]string, map[string]string)
}

type IssueShow struct{}

func (i IssueShow) Action(wg *sync.WaitGroup, issue Issue, filters map[string]string, options map[string]string, results chan interface{}) {
	defer func() {
		results <- nil
		wg.Done()
	}()
	logIssue(issue.IID, "From: ", issue.Author.Username, " (", issue.Author.Name, ") ", "Subject: ", issue.Title)
}
func (i IssueShow) Result([]interface{}, map[string]string, map[string]string) {}

type IssueDummy struct{}

func (i IssueDummy) Action(wg *sync.WaitGroup, issue Issue, filters map[string]string, options map[string]string, results chan interface{}) {
	defer func() {
		results <- nil
		wg.Done()
	}()
}
func (i IssueDummy) Result([]interface{}, map[string]string, map[string]string) {}

type IssueDelete struct{}

func (i IssueDelete) Action(wg *sync.WaitGroup, issue Issue, filters map[string]string, options map[string]string, results chan interface{}) {
	defer func() {
		results <- nil
		wg.Done()
	}()
	logIssue(issue.IID, "Deleting...")
	err := issue.Delete()
	if err != nil {
		logIssue(issue.IID, "Error while trying to Delete: ", err.Error())
	} else {
		logIssue(issue.IID, "Deleted.")
	}
}
func (i IssueDelete) Result([]interface{}, map[string]string, map[string]string) {}

type IssueStatsReport struct {
	NoteUsers []UserCount
	User      User
}

type IssueStats struct{}

func (i IssueStats) Action(wg *sync.WaitGroup, issue Issue, filters map[string]string, options map[string]string, results chan interface{}) {
	report := IssueStatsReport{}
	defer func() {
		results <- report
		wg.Done()
	}()
	logIssue(issue.IID, "Issue:", issue.IID, " from ", issue.Author.Name)
	var noteUsers []UserCount
	report.User = issue.Author
	// NOTES
	notes, err := issue.GetAllNotes(false)
	if err != nil {
		logIssue(issue.IID, "WARNING: error during notes processing: "+err.Error())
		panic(err.Error())
	}
	re := regexp.MustCompile(`\S+`)
	timeBefore := time.Time{}
	if before, ok := filters[UpdatedBefore]; ok {
		timeBefore, _ = time.Parse(time.RFC3339, before)
	}
	timeAfter := time.Time{}
	if after, ok := filters[UpdatedAfter]; ok {
		timeAfter, _ = time.Parse(time.RFC3339, after)
	}
	for _, note := range *notes {
		if (!timeBefore.IsZero() && note.UpdatedAt.After(timeBefore)) || (!timeAfter.IsZero() && note.UpdatedAt.Before(timeAfter)) {
			logIssue(issue.IID, "Skipping Note ", note.ID, " updated at ", note.UpdatedAt)
			continue
		}
		nbWords := len(re.FindAllString(note.Body, -1))
		logIssue(issue.IID, "One note from ", note.Author.Name, ", nb words: ", nbWords)
		noteUsers = append(noteUsers, UserCount{note.Author, nbWords})
	}
	report.NoteUsers = noteUsers
}

func (i IssueStats) Result(results []interface{}, filters map[string]string, options map[string]string) {
	userMap := map[int64]User{}
	noteUserMap := map[int64]int64{}
	wordsUserMap := map[int64]int64{}
	issueUserMap := map[int64]int64{}
	for _, r := range results {
		report := r.(IssueStatsReport)
		for _, uw := range report.NoteUsers {
			userMap[uw.User.ID] = uw.User
			if _, ok := noteUserMap[uw.User.ID]; ok {
				noteUserMap[uw.User.ID] += 1
				wordsUserMap[uw.User.ID] += int64(uw.Count)
			} else {
				noteUserMap[uw.User.ID] = 1
				wordsUserMap[uw.User.ID] = int64(uw.Count)
			}
		}
		if _, ok := issueUserMap[report.User.ID]; ok {
			issueUserMap[report.User.ID] += 1
		} else {
			issueUserMap[report.User.ID] = 1
		}
	}
	output := "md"
	if o, ok := options["output"]; ok {
		output = o
	}
	switch output {
	case "csv":
		printIssueCSV(filters, options, len(results), userMap, noteUserMap, wordsUserMap, issueUserMap)
	default:
		printIssueMarkdown(filters, options, len(results), userMap, noteUserMap, wordsUserMap, issueUserMap)
	}
}

func printIssueCSV(filters map[string]string, options map[string]string, numberOfIssues int, userMap map[int64]User, noteUserMap map[int64]int64, wordsUserMap map[int64]int64, issueUserMap map[int64]int64) {
	logStdoutln(" After , Before , Username , Type , Value , NumberOf ")
	before := ""
	if b, ok := filters[UpdatedBefore]; ok {
		before = b
	}
	after := ""
	if a, ok := filters[UpdatedAfter]; ok {
		after = a
	}
	for _, item := range sortMap(issueUserMap) {
		uid, nbIssues := item.Key, item.Value
		logStdoutln("\""+after+"\"", " , ", "\""+before+"\"", " , ", "\""+userMap[uid].Username+"\"", " , ", "IssueNumber", " , ", nbIssues, " , ", numberOfIssues)
	}
	for _, item := range sortMap(noteUserMap) {
		uid, nbNote := item.Key, item.Value
		logStdoutln("\""+after+"\"", " , ", "\""+before+"\"", " , ", "\""+userMap[uid].Username+"\"", " , ", "IssueComments", " , ", nbNote, " , ", "")
	}
	for _, item := range sortMap(wordsUserMap) {
		uid, nbWords := item.Key, item.Value
		logStdoutln("\""+after+"\"", " , ", "\""+before+"\"", " , ", "\""+userMap[uid].Username+"\"", " , ", "IssueWords", " , ", nbWords, " , ", "")
	}
}

func printIssueMarkdown(filters map[string]string, options map[string]string, numberOfIssues int, userMap map[int64]User, noteUserMap map[int64]int64, wordsUserMap map[int64]int64, issueUserMap map[int64]int64) {
	logStdoutln("## Issues")
	logStdoutln("")
	before := ""
	if b, ok := filters[UpdatedBefore]; ok {
		before = b
	}
	after := ""
	if a, ok := filters[UpdatedAfter]; ok {
		after = a
	}
	if after != "" {
		logStdoutln("After ", after)
	}
	if before != "" {
		logStdoutln("Before ", before)
	}
	logStdoutln("")
	logStdoutln("")
	logStdoutln("Number of Issues found: ", numberOfIssues)
	logStdoutln("")
	for _, item := range sortMap(issueUserMap) {
		uid, nbIssues := item.Key, item.Value
		logStdoutln(userMap[uid].Name, " (", userMap[uid].Username, ") : ", nbIssues)
	}
	logStdoutln("")
	logStdoutln("### Comments")
	logStdoutln("")
	for _, item := range sortMap(noteUserMap) {
		uid, nbNote := item.Key, item.Value
		logStdoutln(userMap[uid].Name, " (", userMap[uid].Username, ") : ", nbNote)
	}
	logStdoutln("")
	logStdoutln("### Words")
	logStdoutln("")
	for _, item := range sortMap(wordsUserMap) {
		uid, nbWords := item.Key, item.Value
		logStdoutln(userMap[uid].Name, " (", userMap[uid].Username, ") : ", nbWords)
	}
	logStdoutln("")

}

func ActOnIssues(project *Project, filters map[string]string, maxParallel int, actionStr string, options map[string]string) {
	checkDate(filters, UpdatedBefore)
	checkDate(filters, UpdatedAfter)
	issues, err := project.GetIssues(filters, true)
	if err != nil {
		log.Println("Error while retrieving Project's Issues: ", err.Error())
		os.Exit(-1)
	}
	log.Println("Found", len(issues), "Issues in Project Number ", project.ID, "with the following criteria: ")
	log.Println(filters)

	var action IssueAction
	switch actionStr {
	case ActionDummy:
		action = IssueDummy{}
	case ActionShow:
		action = IssueShow{}
	case ActionDelete:
		action = IssueDelete{}
	case ActionStats:
		action = IssueStats{}
	default:
		log.Println("Action", actionStr, "Unknown")
		os.Exit(-1)
	}
	log.Println("")
	log.Println("You asked for Action", bold(actionStr))
	log.Println("")
	if !askConfirm("Do you want to procede?") {
		log.Println("Aborting...")
		os.Exit(-1)
	}
	results := make(chan (interface{}), len(issues))
	for i := 0; i < len(issues); i += maxParallel {
		var sub []Issue
		if i+maxParallel > len(issues) {
			sub = issues[i:]
		} else {
			sub = issues[i : i+maxParallel]
		}
		var wg sync.WaitGroup
		for _, issue := range sub {
			wg.Add(1)
			go action.Action(&wg, issue, filters, options, results)
		}
		wg.Wait()
	}
	var resultArray []interface{}
	for i := 0; i < len(issues); i++ {
		resultArray = append(resultArray, <-results)
	}
	action.Result(resultArray, filters, options)
}
