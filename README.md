# Mr Proper - Gitlab

## Build

```
go build
```

## Run

The following documentation is assuming you know your gitlab project identifier: just look at the Home page of your project, you should find a `Project ID: XXXXX` item.

Next, you need to generate a valid [Gitlab private token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

Then, you need to provide this token through the `GITLAB_PRIVATE_TOKEN` environment variable.

You can also override the default gitlab API root URL with the `GITLAB_API_ROOT_URL` environment variable.

```shell
export GITLAB_PRIVATE_TOKEN=<your private token here>
export GITLAB_API_ROOT_URL=<your gitlab API root URL> # Default is https://code.videolan.org/api/v4
./mrproperlab
```

